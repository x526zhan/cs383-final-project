import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

import java.io.File;
import java.util.Arrays;
import java.util.Objects;
import java.io.FilenameFilter;

import websockets.*;

WebsocketServer socket;

import processing.sound.*;
import processing.video.*;
import org.openkinect.processing.*;
import gifAnimation.*;
import http.requests.*;

ArrayList<String> messages;
String init_path;

// UI
String profile_pic_path = "";
String images_path = "images/";
int num_messages_shown = 3;
float profile_size = 30;
PImage background_img;
PImage profile_img;
PImage bottom_bar_comment;
PImage bottom_bar_voice;
Gif voice_icon;
PImage live_img;
PFont text_font;

// Silhouette
Kinect2 kinect2;
PImage depthImg;

// Which pixels do we care about?
int minDepth =  0;
int maxDepth =  4500; //4.5m

// What is the kinect's angle
float angle;
	
// Voice detection
Amplitude amp;
AudioIn in;
float voice_threshold = 0.015;

float prob = 0.005;
float rand_num;
int start_time;
int time_interval = 50;
int glitch_frames = 3;
int count_frame = 0;

// Video playing
String movie_path = "videos/";
ArrayList<Movie> movies;
ArrayList<String> movie_paths;
Movie cur_movie;
boolean[] play_movie;
int start_movie_idx = 0;
int prev_movie_idx = 0;
int cur_movie_idx = 0;
float movie_end_duration = 0.12;
float screen_portion = 0.8;
float margin_portion = 0.02;

ArrayList<String> emoji_paths;
ArrayList<Emoji> emojis;
int emoji_size = 60;
int survival_time = 20;

class Emoji {
    void setup() {
        size(200,200);
    }

    Emoji(int idx) {
        this.idx = idx;
        this.speed = 8;
        this.emoji_img = loadImage(init_path + images_path + emoji_paths.get(idx));
        this.pos_x = screen_portion*width*0.5 + bottom_bar_comment.width * 1/3 + emoji_size * 1.5 * idx;
        this.pos_y = (1 + screen_portion)*0.5*height - bottom_bar_comment.height;
        this.emoji_start_time = millis();
    }

    void display() {
        if (millis() < emoji_start_time + 1000 * survival_time) {
            float transparency = 1 - (millis() - emoji_start_time) / float(1000 * survival_time);
            tint(255, (int)(255 * transparency));
            image(emoji_img, pos_x, pos_y, emoji_size, emoji_size);
            noTint();
        }
    }

    void move() {
        // Jiggle left & right but move upwards
        pos_x = pos_x + random(-0.5,0.5) * speed;
        pos_y = pos_y - random(0,1) * speed;
    }

    int emoji_start_time;
    int idx;
    float speed;
    float pos_x;
    float pos_y;
    PImage emoji_img;
} 

// For voice-text conversion
void setup() {
    noCursor();
    fullScreen();

    // Set up Kinect
    kinect2 = new Kinect2(this);
    kinect2.initDepth();
    kinect2.initDevice();

    // Blank image
    depthImg = new PImage(kinect2.depthWidth, kinect2.depthHeight);

    // Initialize set of videos to loop
    init_path = sketchPath() + "/data/";
    movies = new ArrayList<Movie>();
    movie_paths = new ArrayList<String>();
    loadMovies();
    movies.get(cur_movie_idx).play();
    frameRate(120);

    // Initialize UI
    String abs_img_path = init_path + images_path;
    background_img = loadImage(abs_img_path + "fb-background.png");
    profile_img = loadImage(abs_img_path + "fb-profile.png");
    bottom_bar_comment = loadImage(abs_img_path + "bottom-bar-comment.png");
    bottom_bar_comment.resize((int)(screen_portion * width), 0);
    bottom_bar_voice = loadImage(abs_img_path + "bottom-bar.png");
    bottom_bar_voice.resize((int)(screen_portion * width), 0);
    voice_icon = new Gif(this, abs_img_path + "voice-dots-resized.gif");
    voice_icon.play();

    live_img = loadImage(init_path + images_path + "live-btn.png");
    live_img.resize((int)(profile_size*2),0);
    text_font = createFont("HelveticaNeue", 24);

    // Initialize emojis
    emoji_paths = new ArrayList<String>();
    emoji_paths.add("like.png");
    emoji_paths.add("love.png");
    emoji_paths.add("laugh.png");
    emoji_paths.add("wow.png");
    emoji_paths.add("angry.png");

    emojis = new ArrayList<Emoji>();

    // Create an Input stream which is routed into the Amplitude analyzer
    amp = new Amplitude(this);

    // Set it to 1 for Scarlett Mic Interface
    in = new AudioIn(this, 0);
    in.start();
    amp.input(in);

    // Voice-text conversion
    socket = new WebsocketServer(this, 1337, "/p5websocket");
    messages = new ArrayList<String>();

    // Run on Node.js
    link("http://localhost:3000");
    start_time = millis();
}

void loadMovies() {
    ArrayList<String> movie_names = listFileNames(init_path + movie_path, "mp4");
    for (int i = 0; i < movie_names.size(); i++) {
        movies.add(new Movie(this, init_path + movie_path + movie_names.get(i)));
        movie_paths.add(init_path + movie_path + movie_names.get(i));
    }
}

ArrayList<String> listFileNames(String dir, String ext) {
    File file = new File(dir);
    if (file.isDirectory()) {
        ArrayList<String> names = new ArrayList<String>(Arrays.asList(file.list()));
        if (!Objects.equals(ext, "")) {
            for (int i = 0; i < names.size(); i++) {
                String[] exts = names.get(i).split("\\.");
                if (!Objects.equals(exts[exts.length - 1], ext))
                    names.remove(i);
            }
        }
        return names;
    }
    return null;
}

void webSocketServerEvent(String msg) {
    // Note that there may be leading spaces, we trim those
    String new_msg = msg.trim();
    messages.add(capitalize_string(new_msg));
    int idx = -1;

    // Sentiment analysis
    PostRequest post = new PostRequest("http://text-processing.com/api/sentiment/");
    post.addData("text", new_msg);
    post.send();
    JSONObject json = parseJSONObject(post.getContent());
    if (json == null) {
        println("JSONObject could not be parsed");
    } else {
        JSONObject probs = json.getJSONObject("probability");
        float pos_prob = probs.getFloat("pos");
        if (pos_prob > 0.75) idx = 2;
        else if (pos_prob > 0.6) idx = 1;
        else {
            String label = json.getString("label");
            if (Objects.equals(label, "pos")) idx = 0;
            if (Objects.equals(label, "neg")) {
                float neutral_prob = probs.getFloat("neutral");
                if (neutral_prob < 0.35) idx = 4;
            }
        }

        if (new_msg.contains("like")) idx = 0;
        if (new_msg.contains("cool")) idx = 0;
        if (new_msg.contains("good")) idx = 0;
        if (new_msg.contains("great")) idx = 0;
        if (new_msg.contains("awesome")) idx = 0;
        if (new_msg.contains("excite")) idx = 0;
        if (new_msg.contains("wholesome")) idx = 0;
        if (new_msg.contains("cute")) idx = 1;
        if (new_msg.contains("love")) idx = 1;
        if (new_msg.contains("adorable")) idx = 1;
        if (new_msg.contains("ha ")) idx = 2;
        if (new_msg.contains("haha")) idx = 2;
        if (new_msg.contains("happy")) idx = 2;
        if (new_msg.contains("fun")) idx = 2;
        if (new_msg.contains("wow")) idx = 3;
        if (new_msg.contains("shock")) idx = 3;
        if (new_msg.contains("surprise")) idx = 3;
        if (new_msg.contains("oh my")) idx = 3;
        if (new_msg.contains("angry")) idx = 4;
        if (new_msg.contains("hate")) idx = 4;
        if (new_msg.contains("suck")) idx = 4;
        if (new_msg.contains("mad")) idx = 4;
        if (new_msg.contains("unhappy")) idx = 4;
        if (new_msg.contains("anger")) idx = 4;

        if (idx >= 0) emojis.add(new Emoji(idx));
    }
}

void draw() {
    imageMode(CORNER);
    image(background_img,0,0,width,height);

    noTint();
    imageMode(CENTER); 
    rand_num = millis() > start_time + 1000 * time_interval ? random(1) : 1;
    image(movies.get(cur_movie_idx),width/2,height/2,width*screen_portion,height*screen_portion);

    if (count_frame > glitch_frames) count_frame = 0;

    if (rand_num < prob || count_frame > 0) {
        count_frame++;
        tint(255, 128);
        image(kinect2.getDepthImage(), width/2,height/2,width*screen_portion,height*screen_portion);

        // Threshold the depth image
        int[] rawDepth = kinect2.getRawDepth();

        for (int i=0; i < rawDepth.length; i++) {
            if (rawDepth[i] >= minDepth && rawDepth[i] <= maxDepth) {
                depthImg.pixels[i] = color(255);
            } else {
                depthImg.pixels[i] = color(0);
            }
        }

        // Draw the thresholded image
        depthImg.updatePixels();
        image(depthImg, width/2,height/2,width*screen_portion,height*screen_portion);

        // LIVE symbol
        noTint();
        imageMode(CENTER);
        image(live_img, width/2, (1 - screen_portion)*height*0.5
                + margin_portion*screen_portion*height+profile_size*0.6-2);
    }

    // Show comments
    // If no voice -- show placeholder "Say something..." in comment bar
    // Otherwise -- show dots that indicates speaking
    imageMode(CORNER);
    if (amp.analyze() < voice_threshold) {
        image(bottom_bar_comment, (1 - screen_portion)*width*0.5,
                (1 + screen_portion)*0.5*height - bottom_bar_comment.height);
    }
    else {
        image(bottom_bar_voice, (1 - screen_portion)*width*0.5,
                (1 + screen_portion)*0.5*height - bottom_bar_comment.height);
        imageMode(CENTER);
        tint(255, 170);
        image(voice_icon, (1 - screen_portion)*width*0.5 + margin_portion*screen_portion*width
                + 0.6*voice_icon.width, (1 + screen_portion)*0.5*height - 0.5*bottom_bar_comment.height);
        noTint();
        imageMode(CORNER);
    }

    for (int i = 1; i <= num_messages_shown; i++) {
        if (messages.size() < i) break;
        show_comment(i);
    }

    // Display emojis
    for (int i = 0; i < emojis.size(); i++) {
        emojis.get(i).move();
        emojis.get(i).display();
    }
}

String capitalize_string(String str) {
    return str.substring(0, 1).toUpperCase() + str.substring(1);
}

void show_comment(int idx) {
    // Convert profile pic into a circle shape
    float cur_width = (1 - screen_portion) * width / 2 + margin_portion * screen_portion * width;
    float cur_height = (1 + screen_portion) * 0.5 * height - bottom_bar_comment.height
        - margin_portion * screen_portion * height - 0.5 * profile_size - 1.2 * profile_size * (idx - 1);
    imageMode(CENTER);
    image(profile_img,cur_width+0.5*profile_size,cur_height,profile_size,profile_size);
    textFont(text_font);
    fill(255, 255, 255);
    textAlign(LEFT);
    text(messages.get(messages.size() - idx),
            cur_width + 1.5*profile_size,
            cur_height-0.5*profile_size,
            screen_portion*width*2/3,
            profile_size);
}

void movieEvent(Movie m) {
    if (m.available()) m.read();
    if((m.time() + movie_end_duration) >= m.duration()){
        cur_movie_idx = cur_movie_idx + 1;
        if (cur_movie_idx >= movies.size()) cur_movie_idx = 0;
        movies.set(cur_movie_idx, new Movie(this, movie_paths.get(cur_movie_idx)));
        if (movies.size() == 1) movies.get(cur_movie_idx).loop();
        else movies.get(cur_movie_idx).play();
    }
}
